AMaLa
======
Pseudo-likelihood implementation in [Julia](https://www.julialang.org/) of the AMaLa inference
method.

Install
------
To install the package via the Julia package manager type:
```
(v1.?) pkg> add https://gitlab.com/luca.sesta/Amala
```
Overview
--------

The code is organized in many building blocks.

- `learn.jl`: defining the different inference methods.
- `parameters.jl`: parameters initialization and conversion.
- `workspace.jl`: data framework.
- `optimize.jl`: optimization wrapper.
- `gradient.jl`: functions for gradient computation.

Usage
-----

To load the package type:
```
julia> using Amala
```

The functions performing the inference are defined by the
different methods of `learn_par`. The elementary method takes
as input a specific data structure (see example notebook) which
encodes the information of T alignments, a wild-type sequence NTuple
and two T-dimensional vectors beta and nu.

Inference is based on an implementation of a pseudo-likelihood
parallel maximization algorithm. To employ multiple threading
start julia with  the `-t` option or set the `JULIA_NUM_THREADS`
environment variable. For more information on how set correctly
the number of threads, please refer to the online
[Julia Documentation on Multi-Threading](https://docs.julialang.org/en/v1/manual/multi-threading/).
Optimization is realized via [NLopt](https://github.com/JuliaOpt/NLopt.jl) package.
Multiple gradient based algorithm can be chosen. By default we set `:LD_LBFGS`.

Additional methods of `learn_par` concern: determination of optimal
mutation probability, optimal beta vector (either by scan or alternate
gradient descent).

Output
------
Standard output inference (here name `res`) includes:

* `res.ParMat`: the `L x (L-1)*q*q + q` matrix of parameters, as they are inferred from the asymmetric approach.
* `res.JE`: tensorized couplings of dimension `L x L x q x q`.
* `res.hE`: tensorized fields of dimension `L x q`.
* `res.psl`: site by site pseudo-likelihood.

Notebook
------
An example `jupyter` notebook [examp_note.ipynb](example_note/examp_note.ipynb) is provided to show the inference standard pipeline.
A synthetic dataset is also attached [data.jld2](example_note/data.jld2).



