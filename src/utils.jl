# Sheet code for importing alignment files in
# fasta format.

using FastaIO

# Function that takes a set of input fasta files with repeated sequences.
# INPUT: vector of fasta file names.
# OUTPUT: alignemnt of unique sequences `Z_tot`, S×T matrix `counts`` with
# S number of unique sequences and T number of rounds.

function ReadData(file_names::Vector{String}; max_gap_fraction::Float64=1.0)

    T = length(file_names)
    N = 0

    seq_dict = Vector{Dict{Vector{Int}},Int}(undef,T)
    seqs = Vector{Int}[]

    for t=1:T
        seq_dict[t], L = seq_count_dic(file_names[t]; max_gap_fraction=max_gap_fraction)
        if t==1
            N = L
        else
            @assert N == L
        end
        push!(seqs,keys(seq_dict[t])...)
    end

    seqs = unique(seqs)
    S = length(seqs)

    Z_tot = zeros(Int,N,S)
    counts = zeros(Int,S,T)

    for s=1:S
        Z_tot[:,s] .= Int8.(seqs[s])
    end

    for t=1:T
        for s=1:S
            seq=seqs[s]
            haskey(seq_dict[t],seq) ?  counts[s,t]= seq_dict[t][seq] : counts[s,t]=0;
        end
    end

    return Z_tot, counts

end

# Function that takes a FASTA file as input and provides
# a dictionary having sequences as keys and associated counts
# as values.
function  seq_count_dic(fname::String; max_gap_fraction=1.0)

    Z = fasta2number(fname; max_gap_fraction=max_gap_fraction)
    N, M = size(Z)
    dict = Dict{Vector{Int},Int}()

    for m=1:M
        haskey(dict,Z[:,i]) ?  dict[Z[:,i]]+=1 : dict[Z[:,i]]=1
    end

    return dict, N

end



# Dictionary to map a.a.'s to integer numbers
const AA2INT = Dict('A' => 1, 'C' => 2, 'D' => 3, 'E' => 4, 'F' => 5,
                   'G' => 6, 'H' => 7, 'I' => 8, 'K' => 9, 'L' => 10,
                   'M' => 11, 'N' => 12, 'P' => 13, 'Q' => 14, 'R' => 15,
                   'S' => 16, 'T' => 17, 'V' => 18, 'W' => 19, 'Y' => 20,
                   '-' => 21)

#read fasta in numeric format
fasta2number(filename; max_gap_fraction::Float64=1.0) = read_fasta_alignment(filename; max_gap_fraction=max_gap_fraction)

# Function to return numerical alignment matrix
function read_fasta_alignment(filename::AbstractString; max_gap_fraction::Real=1.0)

    f = FastaReader(filename)

    max_gap_fraction = Float64(max_gap_fraction)

    # pass 1

    seqs = Int[]
    inds = Int[]
    fseqlen = 0

    for (name, seq) in f
        ngaps = 0
        if f.num_parsed == 1
            ls = length(seq)
            resize!(inds, ls)
            for i = 1:ls
                c = seq[i]
                if c != '.' && c == uppercase(c)
                    fseqlen += 1
                    inds[fseqlen] = i
                    c == '-' && (ngaps += 1)
                end
            end
        else
            ls = length(seq)
            ls == length(inds) || error("inputs are not aligned")
            tstfseqlen = 0
            for i = 1:ls
                c = seq[i]
                if c != '.' && c == uppercase(c)
                    tstfseqlen += 1
                    inds[tstfseqlen] == i || error("inconsistent inputs")
                    c == '-' && (ngaps += 1)
                end
            end
            tstfseqlen == fseqlen || error("inconsistent inputs")
        end
        ngaps / fseqlen <= max_gap_fraction && push!(seqs, f.num_parsed)
    end

    length(seqs) > 0 || error("Out of $(f.num_parsed) sequences, none passed the filter (max_gap_fraction=$max_gap_fraction)")

    # pass 2
    Z = Array{Int64}(undef, fseqlen, length(seqs))

    seqid = 1
    for (name, seq) in f
        seqs[end] < f.num_parsed && break
        seqs[seqid] == f.num_parsed || continue
        for i = 1:fseqlen
            c = seq[inds[i]]
            Z[i, seqid] = letter2num(c)
        end
        seqid += 1
    end
    @assert seqid == length(seqs) + 1

    close(f)

    return Z
end

# Function converting a.a. char symbol to number
let alphabet = [ 1,21, 2, 3, 4, 5, 6, 7, 8,21, 9,10,11,12,21,13,14,15,16,17,21,18,19,21,20]
               # A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y
    global letter2num
    function letter2num(c::Union{Char,UInt8})
        i = UInt8(c) - 0x40
        1 <= i <= 25 && return alphabet[i]
        return 21
    end
end

function str2seq(str::AbstractString; dic2int::Dict = AA2INT, q_other::Int = 21) 
    
    s=[]
    for a in str
        if haskey(dic2int,a)
            push!(s,dic2int[a])
        else
            push!(s,q_other)
        end
    end
    return Tuple(s)
end

function energy(seq::T, h::Array{Float64,2}, J::Array{Float64,4}) where T <: Tuple

    @assert size(h,1) == size(J,1)
    @assert size(h,2) == size(J,3)

    q = size(h,1)
    L = size(h,2)

    e = 0.0

    for i=1:L-1
        e -= h[seq[i],i]
        for j=i+1:L
            e -= J[seq[i],seq[j],i,j]
        end
    end
    e -= h[seq[L],L]

    return e

end