module Amala

using SharedArrays,Printf,LinearAlgebra,Statistics,
    IterTools,Random,JLD2, NLopt

    export learn_par

    include("workspace.jl")
    include("parameters.jl")
    include("optimize.jl")
    include("gradient.jl")
    include("learn.jl")
    include("utils.jl")
    
end
