using FastaIO

#Sheet code defining the various inference functions

# Computing Jᴱ, hᴱ parameters of E in β(t)*E-ν(t)∑δ(σᵢ,σᵢᵂᵀ)
# INPUT: vector of fasta file names for each sequenced round, fasta file for wt sequence,
# vector of beta with T elements (round 1 2 .. T) and mutation rate μ.
# OUTPUT: E parameters (Jᴱ and hᴱ).

"""

	learn_par(filenames::Vector{String}, wt_seq::String, beta::Vector{Float64}, mu::Float64, idxr::Vector{Int}; kwds...)
Inference of the energy parameters {Jᴱ,hᴱ} given a set of rounds (t₁,t₂,...,T). Fasta files for each
sequenced round and wild type sequence are provided as input, together with the time dependent vector
β(t), the mutation rate μ and the vector of round times idxr.

Return total minus pseudolikelihood, tensor and matrix like parameters

Optional arguments:
* `max_gap_fraction::Real = 1.0` maximum fraction of gap in aligned sequences
* `lambdaJE::Real = 0.01` coupling regularization multiplier
* `lambdaJE::Real = 0.01` field regularization multiplier
* `weight::Symbol = :counts` how to weight sequences in psl
* `init::Symbol = :ParStart` initiliaze parameter with matrix ParStart
* `epsconv::Real = 1.0e-10` convergence parameter of the optimization algorithm
* `maxit::Int = 10000` maximum number of iteration of the optimization algorithm
* `verbose::Bool = true` determines whether to print optimization output
* `algorithm::Symbol = :LD_LBFGS` specfic NLopt optimization algorithm

#Examples
```
julia> res = learn_par(filenames,wt_fasta,beta,mu,idxr; lambdaJE=0.01, lambdaHE=0.01)
```

"""
function  learn_par(filenames::Vector{String}, wt_fasta::String, beta::Vector{Float64}, mu::Float64, idxr::Vector{Int};
						max_gap_fraction::Real=1.0,
						lambdaJE::Real = 0.01,
						lambdaHE::Real = 0.01,
						weight::Symbol = :counts,
						init::Symbol = :ParStart,
                        epsconv::Real = 1.0e-10,
						maxit::Int = 10000,
						verbose::Bool = true,
						algorithm::Symbol = :LD_LBFGS)

	wt_seq = str2seq(readfasta(wt_fasta)[1][2])
	N = length(wt_seq)

	w = Workspace(filenames, wt_seq, idxr; max_gap_fraction=max_gap_fraction, weight=weight,
					lambdaJE=lambdaJE,lambdaHE=lambdaHE, algorithm = algorithm, verbose = verbose,
					epsconv = epsconv, maxit = maxit)

	p = (w.q-1)/w.q * (1.0 - exp(-mu))
	nu = [comp_JC(w.q,p,t) for t in idxr]

	learn_par(w,beta,nu; q=w.q, N=w.N, init=init, ParStart=zeros(Float64,w.LL,N))

end

# learn_par() overload for automatic β initialization
learn_par(filenames::Vector{String}, wt_fasta::String, mu::Float64, idxr::Vector{Int}; kwds...) = learn_par(filenames,wt_fasta,Float64.([t/data.idxr[1] for t in idxr]),mu,idxr; kwds...)

# Computing Jᴱ, hᴱ parameters of E in β(t)*E-ν(t)∑δ(σᵢ,σᵢᵂᵀ)
# INPUT: vector of fasta file names for each sequenced round, fasta file for wt sequence,
# vector of beta with T elements (round 1 2 .. T) and mutation rate μ.
# OUTPUT: E parameters (Jᴱ and hᴱ).

"""

	learn_par(filename::String, count_file::String, wt_seq::String, beta::Vector{Float64}, mu::Float64, idxr::Vector{Int}; kwds...)
Inference of the energy parameters {Jᴱ,hᴱ} given a set of rounds (t₁,t₂,...,T). Fasta files for all
unique sequence and wild type sequence are provided as input, together with the counts file, time
dependent vector β(t), the mutation rate μ and the vector of round times idxr.

Return total minus pseudolikelihood, tensor and matrix like parameters

Optional arguments:
* `max_gap_fraction::Real = 1.0` maximum fraction of gap in aligned sequences
* `lambdaJE::Real = 0.01` coupling regularization multiplier
* `lambdaJE::Real = 0.01` field regularization multiplier
* `weight::Symbol = :counts` how to weight sequences in psl
* `init::Symbol = :ParStart` initiliaze parameter with matrix ParStart
* `epsconv::Real = 1.0e-10` convergence parameter of the optimization algorithm
* `maxit::Int = 10000` maximum number of iteration of the optimization algorithm
* `verbose::Bool = true` determines whether to print optimization output
* `algorithm::Symbol = :LD_LBFGS` specfic NLopt optimization algorithm

#Examples
```
julia> res = learn_par(filenames,wt_fasta,beta,mu,idxr; lambdaJE=0.01, lambdaHE=0.01)
```

"""
function  learn_par(filename::String, count_file::String, wt_fasta::String, beta::Vector{Float64}, mu::Float64, idxr::Vector{Int};
						max_gap_fraction::Real=1.0,
						lambdaJE::Real = 0.01,
						lambdaHE::Real = 0.01,
						weight::Symbol = :counts,
						init::Symbol = :ParStart,
                        epsconv::Real = 1.0e-10,
						maxit::Int = 10000,
						verbose::Bool = true,
						algorithm::Symbol = :LD_LBFGS)

	wt_seq = str2seq(wt_fasta)
	N = length(wt_seq)

	w = Workspace(filename, count_file, wt_seq, idxr; max_gap_fraction=max_gap_fraction, weight=weight,
					lambdaJE=lambdaJE, lambdaHE=lambdaHE, algorithm=algorithm, verbose=verbose,
					epsconv=epsconv, maxit=maxit)

	@assert N == w.N

	p = (w.q-1)/w.q * (1.0 - exp(-mu))
	nu = [comp_JC(w.q,p,t) for t in idxr]

	learn_par(w,beta,nu; q=w.q, N=w.N, init=init, ParStart=zeros(Float64,w.LL,N))

end

# learn_par() overload for automatic β initialization
learn_par(filename::String, count_file::String, wt_fasta::String, mu::Float64, idxr::Vector{Int}; kwds...) = learn_par(filename,count_file,wt_fasta,Float64.([t/data.idxr[1] for t in idxr]),mu,idxr; kwds...)

# Computing Jᴱ, hᴱ parameters of E in β(t)*E-ν(t)∑δ(σᵢ,σᵢᵂᵀ)
# INPUT: alignment of all unique sequences `Z`` mapped on integers,
# matrix of `counts`, `wt_seq`` sequence of integers,vector of `beta` with T
# elements (round 1 2 .. T), single site mutation probability `p`
# and round indeces `idxr`.
# OUTPUT: E parameters (Jᴱ and hᴱ).

function  learn_par(Z::Matrix{Int}, counts::Matrix{Int}, wt_seq::NTuple, beta::Vector{Float64}, p::Float64, idxr::Vector{Int};
						lambdaJE::Float64 = 0.01,
						lambdaHE::Float64 = 0.01,
						weight::Symbol = :counts,
						init::Symbol = :ParStart,
                        epsconv::Float64 = 1.0e-10,
						maxit::Int = 10000,
						verbose::Bool = true,
						algorithm::Symbol = :LD_LBFGS)

	@assert length(beta) == length(idxr)
	N = length(wt_seq)

	w = Workspace(Z, counts, wt_seq, idxr; weight=weight, lambdaJE=lambdaJE, lambdaHE=lambdaHE, algorithm=algorithm, verbose=verbose,
					epsconv=epsconv, maxit=maxit)

	@assert N == w.N
	nu = [comp_JC(w.q,p,t) for t in idxr]

	learn_par(w,beta,nu; q=w.q, N=w.N, init=init, ParStart=zeros(Float64,w.LL,N))

end

# Overload for automatic beta initialization
learn_par(Z::Matrix{Int}, counts::Matrix{Int}, wt_seq::NTuple, p::Float64, idxr::Vector{Int}; kwds...) = learn_par(Z, counts, wt_seq, [t/idxr[1] for t in idxr], p, idxr; kwds...)

# Computing Jᴱ, hᴱ parameters of E in H(t) = β(t)*E - ν(t)∑δ(σᵢ,σᵢᵂᵀ)
# INPUT: workspace structure, vector of beta with T elements (rounds t₁,t₂,…,T)
# and vector of ν's (rounds t₁,t₂,…,T).
# OUTPUT: E parameters (Jᴱ and hᴱ).

"""
	learn_par(w::Workspace, beta::Vector{Float64}, nu::Vector{Float64}; kwds...)
Inference of the energy parameters {J,h} given a set of rounds (t₁,t₂,...,T). The input is provided by the
workspace data structure and time dependent vectors β(t) and ν(t). Hyperparameters related to the inference
algorithm are embedded into workspace structure.

Return total minus pseudolikelihood, tensor and dmatrix like parameters

Optional arguments:
* `q::Int = w.q` number of amino acids symbol
* `N::Int = w.N` length of the protein sequence
* `init::Symbol = :ParStart` initiliaze parameter with matrix ParStart
* `ParStart::Matrix{Float64} = zeros(Float64,(N-1)*q*q+q,N)` initialization matrix by default set to zero

#Examples
```
julia> res = learn_par(w,beta,nu)
```

"""
function  learn_par(w::Workspace, beta::Vector{Float64}, nu::Vector{Float64};
						q::Int=w.q,
						N::Int=w.N,
						init::Symbol = :ParStart,
						ParStart::Matrix{Float64}=zeros(Float64,w.LL,N))

	if (w.T) != length(beta)
		throw(DimensionMismatch("beta elements does not match rounds"))
	end

	if (w.T) != length(nu)
		throw(DimensionMismatch("nu elements does not match rounds"))
	end

	#learn model
	x = initialization(init, w.samples[w.T], w.LL, w.q, w.N, ParStart)
	x, pslike, fail = optimize(x, w, beta, nu; w.opt_args...)

	JE,hE = Par2Tensor(x, w.q, w.N)

	return (ParMat = x, JE = JE, hE = hE , psl = pslike)
end


# Inference of selective parameters {Jᴱ,hᴱ} for a grid of beta values
# INPUT: workspace data structure, ν vector and for each β element a range of values
# OUTPUT: table of pseudo-likelihood values for each grid point

"""
	learn_par(w::Workspace, betagrid::Vector{Vector{Float64}}, nu::Vector{Float64}; kwds...)
Inference of the energy parameters {J,h} given a set of rounds (t₁,t₂,...,T) for every β vector belonging to betagrid.
The input is provided by the workspace data structure, the grid of β vectors and time dependent vector ν(t). Hyperparameters related
to the inference algorithm are embedded into workspace structure.

Return a table with the total minus pseudolikelihood for every point of the grid

Optional arguments:
* `q::Int = w.q` number of amino acids symbol
* `N::Int = w.N` length of the protein sequence
* `init::Symbol = :ParStart` initiliaze parameter with matrix ParStart
* `ParStart::Matrix{Float64} = zeros(Float64,(N-1)*q*q+q,N)` initialization matrix by default set to zero

#Examples
```
julia> res = learn_par(w,betagrid,nu)
```

"""
function  learn_par(w::Workspace, betagrid::Vector{Vector{Float64}}, nu::Vector{Float64};
						init::Symbol = :ParStart,
						ParStart::Matrix{Float64}=zeros(Float64,w.LL,w.N))

	if (w.T)-1 != length(betagrid)
		throw(DimensionMismatch("beta elements does not match (number rounds -1)"))
	end

	if (w.T) != length(nu)
		throw(DimensionMismatch("nu elements does not match (number rounds)"))
	end

	#learn model for each beta grid values
	x = initialization(init, w.samples[w.T], w.LL, w.q, w.N, ParStart)

    iter = collect.(Iterators.product(betagrid...))
    psl = 0.0
	b = Vector{Float64}(undef,w.T)
	tab = Matrix{Float64}(undef,length(iter),w.T)

    counter = 0
    for i in iter
        counter += 1
        b = vcat(1.0,i)
		println(b)
		x, pslike, fail = optimize(x, w, b, nu; w.opt_args...)
        if 1 in fail
            psl = NaN
        else
            psl= sum(pslike)
		end
		tab[counter,:] = vcat(i,psl)
    end

    return tab
end

function learn_par(Z::Matrix{Int}, counts::Matrix{Int}, wt_seq::NTuple, betagrid::Vector{Vector{Float64}}, idxr::Vector{Int}, p::Float64;
						N::Int=size(Z,1),
						q::Int=round(Int,maximum(Z)),
						lambdaJE::Float64 = 0.01,
						lambdaHE::Float64 = 0.01,
						weight::Symbol = :counts,
						init::Symbol = :ParStart,
                        epsconv::Float64 = 1.0e-10,
						maxit::Int = 10000,
						verbose::Bool = true,
						algorithm::Symbol = :LD_LBFGS,
						ParStart::Matrix{Float64}=zeros(Float64,(N-1)*q*q+q,N))

	@assert size(counts,2) == length(betagrid)+1 == length(idxr)

	@assert N == length(wt_seq)

	w = Workspace(Z, counts, wt_seq, idxr; weight=weight, lambdaJE=lambdaJE, lambdaHE=lambdaHE, algorithm=algorithm, verbose=verbose,
					epsconv=epsconv, maxit=maxit)

	nu = [comp_JC(w.q,p,t) for t in idxr]

	learn_par(w, betagrid, nu; init=init, ParStart=ParStart)


end

# Computing selective parameters {Jᴱ,hᴱ} for a vector of mutation rate.
# INPUT: workspace structure, vector of β(t)'s, time round (t=t₁,t₂,...,T),
# vector of single site mutation probability p=(p₁,p₂,...,pₙ) and filename string.
# OUTPUT: pseudo-likelihood values for each value of p and set of selective
# E parameters (Jᴱ and hᴱ) for the minimum of pseudo-likelihood saved on a file.

"""
	learn_par(w::Workspace, beta::Vector{Float64}, round::Vector{Int64}, p_vec::Vector{Float64},
	filename::String; kwds...)
Inference of the energy parameters {J,h} given a set of round=(t₁,t₂,...,T) for a vector of mutation probabilities p_vec.
The input is provided by the workspace data structure, the vectors β(t), round, p_vec and a filname string. Hyperparameters
related to the inference algorithm are embedded into workspace structure.

Return a table with the total minus pseudolikelihood for element of p_vec, saving on file the parameters related to the minimum

Optional arguments:
* `q::Int = w.q` number of amino acids symbol
* `N::Int = w.N` length of the protein sequence
* `init::Symbol = :ParStart` initiliaze parameter with matrix ParStart
* `ParStart::Matrix{Float64} = zeros(Float64,(N-1)*q*q+q,N)` initialization matrix by default set to zero

#Examples
```
julia> res = learn_par(w,beta,idxr,p_vec,"~/res_popt")
```

"""
function  learn_par(w::Workspace, beta::Vector{Float64}, round::Vector{Int64}, p_vec::Vector{Float64},
					filename::String;
						q::Int=w.q,
						init::Symbol = :ParStart,
						ParStart::Matrix{Float64}=zeros(Float64,w.LL,w.N))


	if (w.T) != length(beta)
		throw(DimensionMismatch("Beta elements does not match rounds"))
	end
	
	if (w.T) != length(round)
		throw(DimensionMismatch("Rounds indeces do not match rounds"))
	end

	psl_p = zeros(Float64,length(p_vec),2)
	psl_p[:,1] .= p_vec
	x = initialization(init, w.samples[w.T], w.LL, w.q, w.N, ParStart)
	counter = 0

	# Optimization for each mutation rate
	for p in p_vec
		counter += 1
		nu = [ comp_JC(q,p,t) for t in round ]
		x, pslike, fail = optimize(x, w, beta, nu; w.opt_args...)

        # Saving first optimization outcome
		if counter == 1
			JE,hE = Par2Tensor(x, w.q, w.N)
			res = (ParMat = x, JE = JE, hE = hE , psl = pslike, p=p)
			@save string(filename,".jld2") res
		end

        # Saving outcome for pseudolikelihood minimum
		if counter > 1 && sum(pslike) < psl_p[counter-1,2]
			JE,hE = Par2Tensor(x, w.q, w.N)
			res = (ParMat = x, JE = JE, hE = hE , psl = pslike, p=p)
			@save string(filename,".jld2") res
		end

		psl_p[counter,2] = sum(pslike)

	end

	return psl_p

end

# Same task as the previous function. This time returns only the
# p_opt coinciding with the extremum of pseudo-likelihood, together
# with the corresponding selective parameters {Jᴱ,hᴱ}.

function  learn_par(w::Workspace, beta::Vector{Float64}, round::Vector{Int64}, p_vec::Vector{Float64};
						init::Symbol = :ParStart,
						ParStart::Matrix{Float64}=zeros(Float64,w.LL,w.N))


	if (w.T) != length(beta)
		throw(DimensionMismatch("Beta elements does not match rounds"))
	end
	
	if (w.T) != length(round)
		throw(DimensionMismatch("Rounds indeces do not match rounds"))
	end

	p_min = 0.0
	JE = zeros(Float64,w.q,w.q,w.N,w.N)
	hE = zeros(Float64,w.q,w.N)
	psl_vec = zeros(Float64,length(p_vec))
	x = initialization(init, w.samples[w.T], w.LL, w.q, w.N, ParStart)
	counter = 0

	# Optimization for each mutation rate
	for p in p_vec
		counter += 1
		nu = [ comp_JC(w.q,p,t) for t in round ]
		x, pslike, fail = optimize(x, w, beta, nu; w.opt_args...)
		psl_vec[counter] = sum(pslike)

        # Saving first optimization outcome
		if counter == 1
			JE,hE = Par2Tensor(x, w.q, w.N)
			p_min = p
		end

        # Saving outcome for pseudolikelihood minimum
		if counter > 1 && sum(pslike) < psl_vec[counter-1]
			JE,hE = Par2Tensor(x, w.q, w.N)
			p_min = p
		end

	end

	return (JE=JE, hE=hE, p=p_min)

end

function learn_par(Z::Matrix{Int}, counts::Matrix{Int}, wt_seq::NTuple, beta::Vector{Float64}, idxr::Vector{Int}, p_vec::Vector{Float64};
						N::Int=size(Z,1),
						q::Int=round(Int,maximum(Z)),
						lambdaJE::Float64 = 0.01,
						lambdaHE::Float64 = 0.01,
						weight::Symbol = :counts,
						init::Symbol = :ParStart,
                        epsconv::Float64 = 1.0e-10,
						maxit::Int = 10000,
						verbose::Bool = true,
						algorithm::Symbol = :LD_LBFGS,
						ParStart::Matrix{Float64}=zeros(Float64,(N-1)*q*q+q,N))

	@assert size(counts,2) == length(beta) == length(idxr)

	@assert N == length(wt_seq)

	w = Workspace(Z, counts, wt_seq, idxr; weight=weight, lambdaJE=lambdaJE, lambdaHE=lambdaHE, algorithm=algorithm, verbose=verbose,
					epsconv=epsconv, maxit=maxit)

	learn_par(w, beta, idxr, p_vec; init=init, ParStart=ParStart)


end

# Function performing a Newton-like alternate gradient descent seeking
# for optimal set of β's.
# INPUT: workspace data structure, ν's vector of T elements (t=t₁,t₂,...,T)
# OUTPUT: optimal β's vector, Jᴱ and hᴱ parameters, pseudolikelihood.

"""
	learn_par(w::Workspace, nu::Vector{Float64}; kwds...)
Inference of the energy parameters {J,h} and optimal β vector, given a set of rounds (t₁,t₂,...,T).
The input is provided by the workspace data structure and a time dependent vector ν(t). Hyperparameters related
to the inference algorithm are embedded into workspace structure.

Return optimal β vector, tensor and matrix parameters and total minus pseudolikelihood

Optional arguments:
* `bstart::Vector{Float64} = [t for t=2:w.T]` initialization for β vector
* `init::Symbol = :ParStart` initiliaze parameter with matrix ParStart
* `skip::Bool = false` if skip==true avoid first parameter optimization
* `betatol::Real = 1.e-3` relative difference between adjacent β to stop the algorithm
* `b_iter::Int = 1` number of GD steps for β
* `ParStart::Matrix{Float64}=zeros(Float64,w.LL,w.N)` energy parameters initialization


#Examples
```
julia> res = learn_par(w,nu; bstart=[t/idxr[1] for t in idxr], betatol=1.0e-4)
```

"""
function  learn_par(w::Workspace, nu::Vector{Float64};
						bstart::Vector{Float64} = [t for t=2:w.T],
						init::Symbol = :ParStart,
						skip::Bool = false,
						betatol::Real = 1.e-3,
						maxit_global::Int = 100,
						b_iter::Int = 1,
						ParStart::Matrix{Float64}=zeros(Float64,w.LL,w.N))

	if (w.T - 1) != length(bstart)
		throw(DimensionMismatch("betastart elements does not match (number rounds - 1)"))
	end

	if (w.T) != length(nu)
		throw(DimensionMismatch("v elements does not match number rounds"))
	end

	x = initialization(init, w.samples[1], w.LL, w.q, w.N, ParStart)
	beta = copy(bstart)
	Dbeta = ones(Float64,w.T-1)
	fail = zeros(Int, w.N)
	counter = 0

	if !skip
		x, pslike, fail = optimize(x, w, vcat(1.0, beta), nu; w.opt_args...)
	end

	println("beta start: ",beta)

	while( any(Dbeta .> betatol) && counter < maxit_global )

			bold=copy(beta);
			BetaOptimize!(beta, nu, x, w, verbose = w.opt_args.verbose, ftol = betatol, maxiter=b_iter)
			Dbeta = abs.(bold.-beta)./beta
			println("> ",beta," (",Dbeta,")\n")

			x, pslike, fail = optimize(x, w, vcat(1.0, beta), nu; w.opt_args...)

			if 1 in fail
				error("Pseudo-likelihood minimization failed")
			end
	        counter += 1
	end

	JE,hE = Par2Tensor(x, w.q, w.N)

	return (beta=beta, ParMat=x, JE=JE, hE=hE, psl=pslike)

end
