using DelimitedFiles

export Workspace

# Structure containing data sample: alignment `Z`, weight vector `W`,
# number of unique sequences `M`, indexing matrix `IdxZ` for fast gradient
# computation.

struct DataSample

    Z:: SharedArray{Int,2}
    W::SharedArray{Float64,1}
    M::Int
    IdxZ::SharedArray{Int,2} #partial index computation for speed up energy calculation

    function DataSample(Z,W,M,IdxZ)
        sZ = SharedArray{Int}(size(Z))
        sZ[:] = Z
        sW = SharedArray{Float64}(size(W))
        sW[:] = W
        sIdxZ = SharedArray{Int}(size(IdxZ))
        sIdxZ[:] = IdxZ

        new(sZ, sW, M, sIdxZ)
    end

end


function DataSample(filenames::Vector{String}; weight::Symbol=:counts, max_gap_fraction::Float64=1.0)

    Z_tot, counts = ReadData(filenames; max_gap_fraction=max_gap_fraction)

    @assert all(0 .≤ counts .< Inf)

    T = size(counts,2)
    ds = DataSample[]

    for t=1:T
        push!(ds,DataSample(Z_tot,counts[:,t]; weight=weight))
    end

    return ds

end

function DataSample(filename::String, count_file::String; weight::Symbol=:counts, max_gap_fraction::Float64=1.0)

    Z_tot = fasta2number(filename; max_gap_fraction=max_gap_fraction)
    counts = readdlm(count_file,Int)

    @assert all(0 .≤ counts .< Inf)

    T = size(counts,2)

    ds = DataSample[]
    for t=1:T
        push!(ds,DataSample(Z_tot,counts[:,t]; weight=weight))
    end

    return ds

end

function DataSample(Z_tot::Matrix{Int}, counts::Vector{Int}; weight::Symbol=:counts)

    N = size(Z_tot,1)
    q = round(Int,maximum(Z_tot))
    idx = findall(counts.>0.)

    M = length(idx)
    Z = Array{Int8}(undef, N, M)
    IdxZ = Array{Int64}(undef, N, M)

    Wt = Array{Float64}(undef,M)

    if weight==:counts
        Wt=counts[idx]
    else
        Wt=ones(M)
    end
    Wt=Wt./sum(Wt)

    q2=q*q

    for i in 1:M
        for j in 1:N
            Z[j,i] = Z_tot[j,idx[i]]
            IdxZ[j,i] = (j-1) * q2 + q * (Z[j,i] - 1)
        end
    end

    DataSample(Z, Wt, M, IdxZ)
end

# Workspace data structure including both sample (DataSample)
# and metaparameters.

struct Workspace

    N::Int #length sequences part
    q::Int #number amino acids symbols
    LL::Int #number of parameters


    T::Int  #number of samples
    idxr::Vector{Int} #Round times

    wt_seq::NTuple#wt sequence

    samples::Vector{DataSample} #data

    reg::NamedTuple #regularizzation

    opt_args::NamedTuple #optimization arguments

    
    function Workspace(N::Int, q::Int, LL::Int, T::Int, idxr::Vector{Int}, wt_seq::NTuple,
            samples::Vector{DataSample}, reg::NamedTuple, opt_args::NamedTuple)
            new(N, q, LL, T, idxr, wt_seq, samples, reg, opt_args)
    end

end

"""
    Workspace(filename::String, count_file::String, wt_seq::NTuple, idxr::Vector{Int}; kwds...)
Global framework including both sample data and metaparameters.
Taking as input file name of the fasta with all unique sequences `filename`,
file with counts `count_file`, wild type sequence `wt_seq` and round times `idxr`.

Returns: sequence length `N`, # of a.a. `q`, # of parameters `LL=(N-1)*q*q+q`,
# of rounds `T`, round time `idxr`, wt sequence `wt_seq`, data sample `samples`,
regularization multipliers `reg`, optimization arguments `opt_arg`.

Optional arguments
* `max_gap_fraction::Real=0.1` maximum fraction of gaps in aligned sequences
* `lambdaJE::Real = 0.01` couplings regularization multiplier
* `lambdaHE::Real = 0.01` fileds regularization multiplier
* `weight::Symbol = :counts` sequence weights are proportioanl to the number of counts
* `algorithm::Symbol = :LD_LBFGS` specific optimization algorithm
* `verbose::Bool = true` if true prints optimization output
* `epsconv::Float64 = 1e-10` convergence criterion
* `maxit::Int = 10000` maximum number of iteration for convergence

#Examples
```
julia> w = Workspace(filename, count_file, wt_seq, idxr; lambdaJE=0.01, lambdaHE=0.01) 
```

"""
function Workspace(filename::String, count_file::String, wt_seq::NTuple, idxr::Vector{Int};
    max_gap_fraction::Real=1.0,
    lambdaJE::Real = 0.01,
    lambdaHE::Real = 0.01,
    weight::Symbol = :counts,
    algorithm ::Symbol = :LD_LBFGS,
    verbose::Bool = true,
    epsconv::Float64 = 1e-10,
    maxit::Int = 10000)

    samples = DataSample(filename, count_file; weight=weight, max_gap_fraction=max_gap_fraction)

    N = size(samples[1].Z,1)
    q = round(Int,maximum(samples[1].Z))
    LL = (N - 1)*q*q + q
    T = length(samples)

    reg = (lambdaJE=lambdaJE, lambdaHE=lambdaHE)

    opt_args = (algorithm = algorithm, verbose = verbose,
                xatol = epsconv*1e2, xrtol = epsconv*1e2,
                fatol = epsconv, frtol = epsconv*1e-2, maxeval = maxit)

    Workspace(N, q, LL, T, idxr, wt_seq,
            samples, reg, opt_args)

end

"""
    Workspace(filenames::Vector{String}, wt_seq::NTuple, idxr::Vector{Int}; kwds...)
Global framework including both sample data and metaparameters.
Taking as input file names of the fasta alignments with repeated sequences
`filenames`, wild type sequence `wt_seq` and round times `idxr`.

Returns: sequence length `N`, # of a.a. `q`, # of parameters `LL=(N-1)*q*q+q`,
# of rounds `T`, round time `idxr`, wt sequence `wt_seq`, data sample `samples`,
regularization multipliers `reg`, optimization arguments `opt_arg`.

Optional arguments
* `max_gap_fraction::Real=0.1` maximum fraction of gaps in aligned sequences
* `lambdaJE::Real = 0.01` couplings regularization multiplier
* `lambdaHE::Real = 0.01` fileds regularization multiplier
* `weight::Symbol = :counts` sequence weights are proportioanl to the number of counts
* `algorithm::Symbol = :LD_LBFGS` specific optimization algorithm
* `verbose::Bool = true` if true prints optimization output
* `epsconv::Float64 = 1e-10` convergence criterion
* `maxit::Int = 10000` maximum number of iteration for convergence

#Examples
```
julia> w = Workspace(filename, count_file, wt_seq, idxr; lambdaJE=0.01, lambdaHE=0.01) 
```

"""
function Workspace(filenames::Vector{String}, wt_seq::NTuple, idxr::Vector{Int};
    max_gap_fraction::Real=1.0,
    lambdaJE::Real = 0.01,
    lambdaHE::Real = 0.01,
    weight::Symbol = :counts,
    algorithm ::Symbol = :LD_LBFGS,
    verbose::Bool = true,
    epsconv::Float64 = 1e-10,
    maxit::Int = 10000)

    samples = DataSample(filenames; weight=weight, max_gap_fraction=max_gap_fraction)

    N = size(samples[1].Z,1)
    q = round(Int,maximum(samples[1].Z))
    LL = (N - 1)*q*q + q
    T = length(samples)

    reg = (lambdaJE=lambdaJE, lambdaHE=lambdaHE)

    opt_args = (algorithm = algorithm, verbose = verbose,
                xatol = epsconv*1e2, xrtol = epsconv*1e2,
                fatol = epsconv, frtol = epsconv*1e-2, maxeval = maxit)

    Workspace(N, q, LL, T, idxr, wt_seq,
            samples, reg, opt_args)

end

"""
    Workspace(samples::Vector{DataSample}, wt_seq::NTuple, idxr::Vector{Int}; kwds...)
Global framework including both sample data and metaparameters.
Taking DataSample vector `samples`, wild type sequence `wt_seq`, round time `idxr`.

Returns: sequence length `N`, # of a.a. `q`, # of parameters `LL=(N-1)*q*q+q`,
# of rounds `T`, round time `idxr`, wt sequence `wt_seq`, data sample `samples`,
regularization multipliers `reg`, optimization arguments `opt_arg`.

Optional arguments
* `lambdaJE::Real = 0.01` couplings regularization multiplier
* `lambdaHE::Real = 0.01` fileds regularization multiplier
* `algorithm::Symbol = :LD_LBFGS` specific optimization algorithm
* `verbose::Bool = true` if true prints optimization output
* `epsconv::Float64 = 1e-10` convergence criterion
* `maxit::Int = 10000` maximum number of iteration for convergence

#Examples
```
julia> w = Workspace(samples, wt_seq, idxr; lambdaJE=0.01, lambdaHE=0.01) 
```

"""
function Workspace(samples::Vector{DataSample}, wt_seq::NTuple, idxr::Vector{Int};
    lambdaJE::Real=0.01,
    lambdaHE::Real=0.01,
    algorithm::Symbol = :LD_LBFGS,
    verbose::Bool = true,
    epsconv::Float64 = 1e-10,
    maxit::Int = 10000)

    T = length(samples)

    N = size(samples[1].Z,1)
    q= Int(maximum(samples[1].Z))
    LL = (N - 1) * q*q + q

    reg = (lambdaJE=lambdaJE, lambdaHE=lambdaHE)

    opt_args = (algorithm = algorithm, verbose = verbose,
                xatol = epsconv*1e2, xrtol = epsconv*1e2,
                fatol = epsconv, frtol = epsconv*1e-2, maxeval = maxit)

    Workspace(N, q, LL, T, idxr, wt_seq,
            samples, reg, opt_args)

end

function Workspace(Z::Matrix{Int}, counts::Matrix{Int}, wt_seq::NTuple, idxr::Vector{Int};
    weight::Symbol=:counts,
    lambdaJE::Float64=0.01,
    lambdaHE::Float64=0.01,
    algorithm::Symbol=:LD_LBFGS,
    verbose::Bool=true,
    epsconv::Float64=1e-10,
    maxit::Int=10000)

    T = size(counts,2)
    ds = [DataSample(Z,counts[:,t]; weight=weight) for t=1:T]

    w = Workspace(ds, wt_seq, idxr; lambdaJE=lambdaJE, lambdaHE=lambdaHE, algorithm=algorithm, verbose=verbose, epsconv=epsconv, maxit=maxit)

    return w

end